//import DS from 'ember-data';

//export default DS.Model.extend({
//  name: DS.attr('string'),
//  books: DS.hasMany('book')
//});

import Publisher from './publisher';
import { hasMany } from 'ember-data/relationships';

export default Publisher.extend({
  books: hasMany('book', { async: true })
});