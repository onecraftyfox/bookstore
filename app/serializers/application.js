import Ember from 'ember';
import DS from 'ember-data';

export default DS.JSONAPISerializer.extend({

  payloadKeyFromModelName(modelName) {
    return Ember.String.singularize(Ember.String.capitalize(modelName));
  }
});
